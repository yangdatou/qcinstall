在不同量子化学程序间传递分子轨道有很多优点，例如：

（1）充分利用不同程序最强、特色的模块，联用不同程序，实现经济性计算，节约机时，或可完成原本无法完成的计算（例如检验ROHF波函数稳定性），甚至可以开发一些QM/MM或ONIOM方法。最简单地，在某程序里做HF计算，到下一个程序中做CCSD(T)计算；在某程序里做CASSCF计算，到下一个程序里做NEVPT2/CASPT2/MRCISD计算，这样无需在下一个程序里再算一遍HF或CASSCF（即使不考虑耗时，能否收敛、能否收敛到一样的解，经常是个麻烦）。

（2）发生SCF不收敛时，不需要尝试添加各种关键词（频繁试错需要大量时间和精力），目前所有量化程序中Gaussian是SCF收敛最好的，在Gaussian中算完了传给其他程序即可，几乎一击必中，节约时间。

利用MOKIT从Gaussian向其他量子化学程序传递分子轨道是MOKIT的特色功能，早已有许多用户使用。笔者在公众号上的各个安装教程里也零散地提到过，但未有一个汇总性的介绍，有些方便的功能用户不一定知道，因此有必要写一篇详细介绍。以水分子的RHF/cc-pVDZ计算为例
```
%chk=h2o.chk
#p RHF/cc-pVDZ nosymm int=nobasistransform

title

0 1
O    0.74608908   -0.25872442    0.0
H    1.70608908   -0.25872442    0.0
H    0.42563449    0.64621141    0.0

```

提交给高斯，算完后运行`formchk h2o.chk h2o.fch`转化生成fch文件。下面我们以h2o.fch为例展示如何传轨道。

此处计算级别RHF/cc-pVDZ仅为示例。在以下所有功能中，不同波函数类型RHF/ROHF/UHF可以自动被识别，球谐型/笛卡尔型基函数也会自动识别。如果用户使用了赝势，赝势数据也会直接写进生成的文件中，关键词会有相应自动调整，给用户省事。个别功能还支持自动转化DFT泛函名称，会在下文中单独提到。提供的文件可以是fch，也可以是chk，若提供后者，本文中的小程序会自动调用高斯的`formchk`先将其转化为fch文件。

## 1. Gaussian传轨道给AMESP
```
fch2amo h2o.fch
```
会产生h2o.amo和h2o.aip两个文件，分别含基组和轨道数据，坐标及关键词。为了让[AMESP](https://www.amesp.xyz)读入轨道，需要运行
```
a2m h2o.amo
```
即将h2o.amo文本文件转化为二进制文件h2o.mo，其中`a2m`是AMESP自带的小程序。

## 2. Gaussian传轨道给BDF
```
fch2bdf h2o.fch
```
会产生H2O.BAS，h2o_bdf.inp和h2o_bdf.scforb三个文件，分别含基组数据，坐标及关键词，轨道数据。

以下命令仅建议十分有经验的用户使用
```
fch2bdf h2o_cas44.fch -no
```
这要求h2o_cas44.fch含有算好的CASSCF自然轨道文件，会产生H2O.BAS，h2o_bdf.inp和h2o_bdf.inporb三个文件，可以到BDF中进行后续如NEVPT2等计算。

## 3. Gaussian传轨道给Dalton
```
fch2dal h2o.fch
```
会产生h2o.dal和h2o.mol两个文件。

## 4. Gaussian传轨道给GAMESS
```
fch2inp h2o.fch
```
会产生h2o.inp文件，内含坐标，基组数据和一些简单的关键词。可能有的小伙伴会使用高斯关键词`punch(mo,gamess)`获取fort.7文件，然后再手动修改为GAMESS输入文件，这种做法有两个缺点：（1）如果使用了赝势，fort.7文件内不含赝势数据，手动补充的话繁琐易错；（2）fort.7文件内含的轨道信息并非GAMESS轨道格式，手动编辑加点数字、加一层`$VEC`，`$END`让其看起来像GAMESS轨道格式并不能解决问题，其没有考虑基函数顺序问题，基组稍微大一些就会发现GAMESS无法在2圈内收敛，或者直接发散（因为传轨道有误）。而使用`fch2inp`小程序则不会有这些问题。

有小伙伴问过笔者`fch2inp`小程序能否用于传轨道到GAMESS里做LMO-EDA或GKS-EDA计算，此问题应使用`frag_guess_wfn`小程序，建议阅读[GKS-EDA计算简介](https://mp.weixin.qq.com/s/6nuJpYJdNbUJndrYM13Fog)。这是因为EDA计算需要传递多套轨道，还有生成关键词，转化泛函名称，设置溶剂参数等步骤，需要用`frag_guess_wfn`从更高一层来调控。

## 5. Gaussian传轨道给Molpro
```
fch2com h2o.fch
```
会产生h2o.a和h2o.com文件，前者含Alpha轨道，后者含坐标，基组和关键词。如果是UHF计算，还会产生h2o.b文件（含Beta轨道）。

## 6. Gaussian传轨道给OpenMolcas
```
fch2inporb h2o.fch
```
会产生h2o.INPORB和h2o.input文件。

## 7. Gaussian传轨道给ORCA
```
fch2mkl h2o.fch
```
会产生h2o_o.mkl和h2o_o.inp文件。前者可用ORCA自带的`orca_2mkl`转化为gbw文件
```
orca_2mkl h2o_o -gbw
```
该文件名默认带`_o`，是由于有的用户会在同一目录下使用多个轨道转化小程序，若都取名为h2o.inp，则容易发生文件覆盖的情况，因此此处加`_o`以区分之。

## 8. Gaussian传轨道给PSI4
```
fch2psi h2o.fch
```
会产生h2o.A和h2o_psi.inp文件。前者含Alpha轨道，后者含坐标，基组和关键词。如果是UHF计算，还会有h2o.B文件（内含Beta轨道）。

## 9. Gaussian传轨道给Q-Chem
```
fch2qchem h2o.fch
```
产生h2o.in文件和一个h2o文件夹。如果机器上有定义环境变量`$QCSCRATCH`，则h2o文件夹会自动被移至Q-Chem临时文件目录下，屏幕上提示你可以运行
```
qchem h2o.in h2o.out h2o
```
提交Q-Chem任务，结果SCF 2圈收敛，能量没有变化。若未定义`$QCSCRATCH`，h2o文件夹则放在当前目录下，读者需要时自行移动。

## 10. Gaussian传轨道给PySCF
该功能较重要，有几种不同使用方式，此处重点介绍。

### 10.1 利用bas_fch2py小程序
运行
```
bas_fch2py h2o.fch
```
可直接生成`h2o.py`文件，内含坐标、基组数据和一些简短语句。也可以偷懒直接运行
```
bas_fch2py h2o.chk
```
小程序`bas_fch2py`会先调用`formchk`小程序将`h2o.chk`转化为`h2o.fch`，省去用户的一个步骤。注意`h2o.py`文件最后3行默认是被注释的
```python
#dm = mf.make_rdm1()
#mf.max_cycle = 10
#mf.kernel(dm)
```
即只读入轨道，不做电子结构计算。若去掉注释，进行计算，可发现RHF/ROHF/UHF方法可在2圈内收敛。若h2o.fch不是由HF计算获得，而是由DFT计算获得，可以改用
```
bas_fch2py h2o.fch -dft
```
则`h2o.py`文件中还会包含PySCF做同一泛函计算的语句。该功能仅支持高斯内置泛函，不支持转化高斯自定义泛函。假设h2o.fch是由B3LYP/6-31G(d)计算而来，此时打开`h2o.py`文件会发现末尾为
```python
dm = mf.make_rdm1()
mf = dft.RKS(mol)
mf.xc = 'b3lypg'
mf.grids.atom_grid = (99,590)
mf.kernel(dm0=dm)
```
即已经帮用户写好了DFT计算语句，且各种设置尽可能模仿Gaussian 16的默认设置。

### 10.2 在.py脚本中直接导入mol对象
此法适合爱好用python编程的人士。我们先看如何导入mol对象，在PySCF里做计算：
```python
from pyscf import scf
from mokit.lib.gaussian import load_mol_from_fch

mol = load_mol_from_fch(fchname='h2o.fch')
mf = scf.RHF(mol).run()
```

可以看到此方式十分简洁，省去很多功夫。如果再配合传轨道，可写为
```python
from pyscf import scf
from mokit.lib.gaussian import load_mol_from_fch, mo_fch2py

fchname = 'h2o.fch'
mol = load_mol_from_fch(fchname)
mf = scf.RHF(mol)
mf.max_cycle = 1
mf.kernel()

mf.mo_coeff = mo_fch2py(fchname)
dm = mf.make_rdm1()
mf.max_cycle = 10
mf.kernel(dm0=dm)
```
第一次SCF跑1圈，让程序开设必要的数组。然后读入轨道，SCF用了2圈收敛且能量基本没变。此处要注意`fchname`文件中的波函数类型应与scf.RHF计算类型对应。假设`fchname`文件中是UHF轨道，那应写`scf.UHF`。

## 注意事项
1. 为使传轨道效果尽可能好，在高斯中做计算产生fch文件时一定要加上关键词`nosymm`，建议还加上`int=nobasistransform`，以免传轨道后两个程序不对应，SCF无法1圈收敛。

2. 获得fch文件一定要用Gaussian做HF/DFT计算么？万一体系大太耗时怎么办？  
答：如果仅想获得fch文件，而不想要里面的分子轨道，那不需要用Gaussian做HF/DFT计算，只需花几秒时间构建一个fch文件即可，例如
```
%chk=h2o.chk
#p RHF/def2TZVP nosymm int=nobasistransform guess(only,save)

title

0 1
O   1.72705309    0.32608695    0.0
H   2.68705309    0.32608695    0.0
H   1.40659850    1.23102278    0.0

```

该文件会在产生SCF初猜后就退出程序，不做电子结构计算，同时可获得chk文件，对大体系耗时一般也就几秒。由此转化得到的fch文件中分子轨道是HF/DFT计算初始轨道，而非收敛轨道，是否想使用之，取决于用户的想法。

另外，MOKIT还有`fchk()`模块可直接从PySCF产生fch文件，见
[《利用MOKIT从PySCF向其他量化程序传轨道》](https://gitlab.com/jxzou/qcinstall/-/blob/main/%E5%88%A9%E7%94%A8MOKIT%E4%BB%8EPySCF%E5%90%91%E5%85%B6%E4%BB%96%E9%87%8F%E5%8C%96%E7%A8%8B%E5%BA%8F%E4%BC%A0%E8%BD%A8%E9%81%93.md)

3. 以上各个小程序几乎都会产生目标程序的输入文件，内含坐标和基组数据，不需要用户再去手动写入基组名称。强烈推荐用户使用该文件进行计算，既能免去手写文件的麻烦，也能保证传轨道时对应性更好。

4. 个别量化程序（如ORCA）仅支持球谐型基函数，不支持笛卡尔型基函数。而在高斯中，在关键词那一行写6-31G(d,p)基组时，默认是"四不像"的6D 7F；又如对所有原子使用SDD时，默认是6D 10F。这些特殊情况普通计算者难以记忆，因此可在计算时统一加上关键词5D 7F，例如
```
%chk=h2o.chk
#p RHF/6-31G(d) 5D 7F nosymm int=nobasistransform

title

0 1
O   1.72705309    0.32608695    0.0
H   2.68705309    0.32608695    0.0
H   1.40659850    1.23102278    0.0

```
如果用户忘记也没关系，MOKIT各个小程序会有警告或报错信息。

5. 利用以上各个小程序传轨道，在下一个量化程序做完计算后，如果有新的轨道产生，可以通过`bdf2fch`，`dal2fch`，`dat2fch`，`mkl2fch`，`mkl2fch`，`orb2fch`等小程序及`py2fch`模块将新的轨道传回fch文件（例如用于GaussView/Multiwfn/VMD可视化），用法与本文类似。

## 相关阅读
[《利用MOKIT从PySCF向其他量化程序传轨道》](https://gitlab.com/jxzou/qcinstall/-/blob/main/%E5%88%A9%E7%94%A8MOKIT%E4%BB%8EPySCF%E5%90%91%E5%85%B6%E4%BB%96%E9%87%8F%E5%8C%96%E7%A8%8B%E5%BA%8F%E4%BC%A0%E8%BD%A8%E9%81%93.md)  
[Amesp中SCF不收敛的解决办法（修订版）](https://mp.weixin.qq.com/s/ZPaIZ8qyYWgTcE6uvR_7QA)  
[离线安装PySCF-2.x](https://gitlab.com/jxzou/qcinstall/-/blob/main/%E7%A6%BB%E7%BA%BF%E5%AE%89%E8%A3%85PySCF-2.x.md)  
[ORCA 5.0安装及运行](https://mp.weixin.qq.com/s/yeCOMhothZeL-V7veAcbuw)  
[GAMESS简易编译教程](https://mp.weixin.qq.com/s/SF5BEfKsGwdKSlZdAe1t4A)  
[PSI4程序安装及运行](https://mp.weixin.qq.com/s/I7Q1YXX5oSsXDe3oMo2jPw)  
[离线安装量子化学软件Dalton](https://mp.weixin.qq.com/s/rsAqeFdrBS3dvgDHFBoYiA)  
[离线安装OpenMolcas-v22.06](https://gitlab.com/jxzou/qcinstall/-/blob/main/%E7%A6%BB%E7%BA%BF%E5%AE%89%E8%A3%85OpenMolcas-v22.06.md)  
[编译MPI并行版OpenMolcas](https://gitlab.com/jxzou/qcinstall/-/blob/main/%E7%BC%96%E8%AF%91MPI%E5%B9%B6%E8%A1%8C%E7%89%88OpenMolcas.md)

