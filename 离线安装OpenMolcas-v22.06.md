本文以OpenMolcas-v23.06为例，对旧版OpenMolcas和v23.10版亦适用。OpenMolcas与QCMaquis联用版本的安装请见文末链接，勿按此文安装。安装前我们需要确保有必要的编译器和库：  
cmake >= 3.12，Intel编译器（含MKL），python  
笔者机子上安装的分别是cmake 3.18.4、Intel OneAPI和Miniconda3。若读者机器上未安装cmake或安装了但版本过低，可以到[官网](https://cmake.org/download/)下载二进制版，解压写好环境变量即可使用。尽管可以使用GNU编译器替代Intel，但笔者的个人使用经验表明用Intel编译器快不少。若读者未装Intel编译器，可以参看[Linux下安装Intel oneAPI](https://mp.weixin.qq.com/s/7pQETkrDO1C83vQjKQqI4w)一文，用旧版的Intel Parallel Studio XE也是可以的。Miniconda3也可以用Anaconda Python3代替。

# 1. 下载
分别到以下3个网址下载压缩包
```
https://gitlab.com/Molcas/OpenMolcas/-/releases
https://gitlab.com/libxc/libxc/-/releases
https://github.com/HDFGroup/hdf5/releases
```

笔者下载的分别是
```
OpenMolcas-v23.06.tar.gz
libxc-5.2.2.tar.gz
hdf5-hdf5-1_14_1-2.tar.gz
```
其中HDF5库对于OpenMolcas不是必须的，但安装后的好处是OpenMolcas在计算完成后会自动产生.h5文件，其可用于后处理、提取数据等，因此建议安装。对于OpenMolcas-v23.10，libxc应下载libxc-6.2.2.tar.gz。

# 2. 安装HDF5
```
tar -zxf hdf5-hdf5-1_14_1-2.tar.gz
cd hdf5-hdf5-1_14_1-2
mkdir build && cd build
CC=icc CXX=icpc FC=ifort cmake -DCMAKE_INSTALL_PREFIX=$HOME/software/hdf5-1.14.1 ..
make -j20
make install
```
其中`-j20`表示用20核并行编译，读者需根据自己机器核数情况修改。这里使用Intel编译器（ifort, icc, icpc）编译HDF5，是因为下一步我们也用Intel编译器编译OpenMolcas，二者最好保持一致。完成后可以看到库文件都在hdf5-1.14.1/目录下，可删除解压出的hdf5-hdf5-1_14_1-2文件夹和压缩包。接着在~/.bashrc文件中导出环境变量
```
export PATH=$HOME/software/hdf5-1.14.1/bin:$PATH
export LD_LIBRARY_PATH=$HOME/software/hdf5-1.14.1/lib:$LD_LIBRARY_PATH
export CPATH=$HOME/software/hdf5-1.14.1/include:$CPATH
```
执行`source ~/.bashrc`或退出重登以使环境变量生效。

# 3. 编译OpenMolcas
进入存放压缩包的目录，解压，配置
```
tar -zxf OpenMolcas-v23.06.tar.gz
cd OpenMolcas-v23.06
mkdir bin build
cd build
CC=icc CXX=icpc FC=ifort cmake -DLINALG=MKL -DOPENMP=ON \
-DCMAKE_INSTALL_PREFIX=$HOME/software/OpenMolcas-v23.06 ..
```

此时我们可以在屏幕上看到HDF5和MKL库有被自动识别出，内容大致如下
```
Configuring HDF5 support:
-- Found HDF5: hdf5-shared (found version "1.14.1") found components: C
-- HDF5_INCLUDE_DIRS: /data/home/jxzou/software/hdf5-1.14.1/include
-- HDF5_C_LIBRARIES: hdf5-shared
Configuring linear algebra libraries:
-- Using Intel Math Kernel Library (MKL)
-- MKLROOT = /data/share/intel/2021.5.0/mkl/2022.0.1
-- MKL_INCLUDE_PATH = /data/share/intel/2021.5.0/mkl/2022.0.1/include
-- MKL_LIBRARY_PATH = /data/share/intel/2021.5.0/mkl/2022.0.1/lib/intel64
-- LINALG_LIBRARIES: /data/share/intel/2021.5.0/mkl/2022.0.1/lib/intel64/libmkl_intel_ilp64.so;/data/share/intel/2021.5.0/mkl/2022.0.1/lib/intel64/libmkl_core.so;/data/share/intel/2021.5.0/mkl/2022.0.1/lib/intel64/libmkl_intel_thread.so
```

接着我们把Libxc放到相应目录下，让OpenMolcas自动识别并编译它。依次执行
```
cd External/Libxc/tmp
rm -f Libxc-gitclone.cmake Libxc-gitupdate.cmake
touch Libxc-gitclone.cmake Libxc-gitupdate.cmake
cd ../src
cp ~/software/libxc-5.2.2.tar.gz .
tar -zxf libxc-5.2.2.tar.gz
rm -rf Libxc libxc-5.2.2.tar.gz
mv libxc-5.2.2 Libxc
cd ../../../
```
注意笔者将libxc-5.2.2.tar.gz压缩包放在~/software/目录下，所以是从~/software/拷贝。若读者放在其他目录下，则应从其他目录拷贝到External/Libxc/src/下。笔者的习惯是将软件放在$HOME/software/目录下，读者也可以放到其他位置。然后编译
```
make -j20
make install
```
正常结束后执行
```
cd ..
mv pymolcas bin/
```
即将脚本pymolcas移入我们之前新建的bin目录里。接着在~/.bashrc中写上OpenMolcas环境变量
```
# OpenMolcas-v23.06
export MOLCAS_WORKDIR=/scratch/$USER/molcas
export MOLCAS_MEM=32Gb
export MOLCAS=$HOME/software/OpenMolcas-v23.06
export PATH=$MOLCAS/bin:$PATH
export MOLCAS_PRINT=3
export MOLCAS_NPROCS=1
export OMP_NUM_THREADS=24
```
完成后记得执行source ~/.bashrc或退出重登。各种路径可以根据读者自己机子的实际情况进行修改。若/scratch/$USER/molcas目录不存在则需手动创建。无论计算任务正常/异常结束，该目录都会有临时文件存在，每隔一段时间应进行清理。若一个任务算完后在相同位置再次提交，OpenMolcas默认去寻找上次的临时文件，好处是可以加速计算，坏处是万一不想要临时文件里的轨道作为初猜，就可能把自己坑了。若想每次计算完自动清空临时文件，可以再加上环境变量
```
export MOLCAS_KEEP_WORKDIR=NO
```
变量MOLCAS_PRINT=3可以让输出内容更多一些，偶尔有小伙伴向笔者反映他们的输出内容比我少，往往就是这个参数造成的。变量MOLCAS_NPROCS用于MPI并行，但本文编译的是MKL并行版，不支持MPI并行，因此设为1。笔者的节点上有24核，因此OMP并行核数设置为24。这些环境变量仅是笔者的个人推荐，并非适用于任何机器，详细的环境变量说明请阅读OpenMolcas手册
```
https://molcas.gitlab.io/OpenMolcas/Manual.pdf
```

# 4. 测试
执行
```
pymolcas --version
```
屏幕应当显示
```
python driver version = py2.24
(after the original perl EMIL interpreter of Valera Veryazov)
```
接着测试程序自带标准示例。最好切换到其他目录进行测试
```
cd ~/
pymolcas verify standard
```
这个standard其实对应OpenMolcas-v23.06/test/standard目录，内含104个输入文件，可供自学模仿使用。测试过程中输出内容仅有一行，例如
```
Running test standard: 067... (68%)
```
测试时间可能长达1小时。如果嫌测试时间长，可以自己挑十几个文件测试，例如测试第004，005号文件的命令是
```
pymolcas verify standard:004,005
```

# 5. OpenMolcas与其他量化程序传递轨道
笔者开发的开源、免费程序[MOKIT](https://gitlab.com/jxzou/mokit)可以在常见量子化学程序间传递分子轨道（见文末链接1，第4节），并调用常见量子化学程序自动做多参考态计算（见文末链接3），本文不再重复介绍。

## 相关阅读
1. [量子化学程序OpenMolcas的简易安装(低于v22.02)](https://mp.weixin.qq.com/s/wA8YClRxkRTChtQvuum-Uw)  
2. [离线编译OpenMolcas+QCMaquis](https://mp.weixin.qq.com/s/Gb1Lzv1bcQmvuHMZjAQLxQ) 
3. [自动做多参考态计算的程序MOKIT](https://mp.weixin.qq.com/s/bM244EiyhsYKwW5i8wq0TQ)

