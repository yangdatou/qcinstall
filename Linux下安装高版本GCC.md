笔者机子上当前gcc版本为4.8.5，欲安装5.4.0版本。  
注意安装高版本gcc可以以普通用户装，也可以以root用户装，且安装目录可以是另外新建的，无需安装进默认的/usr/local, /usr/bin之类的目录（导致覆盖旧的gcc）。因此实际上并不需要“升级gcc”这种操作，只需直接安装一个更高版本即可。过程中用16线程并行编译，共耗时30-40 min。

高版本gcc依赖库要求: GMP 4.2+, MPFR 2.4.0+ 及 MPC 0.8.0+。笔者下载的分别是  
gmp-6.1.0, mpfr-4.0.0, mpc-1.1.0  
注意最好从官网或者一些知名大学的镜像网站（例如[南京大学镜像](http://mirror.nju.edu.cn/gnu)）下载较新版本的GMP，较低版本可能会在装完之后的`make check`时有错误。经笔者尝试，这几个库同样能用于安装gcc-6.5.0和9.2.0。

# 1.安装GMP
解压，编译
```
tar -jxf gmp-6.1.0.tar.bz2
cd gmp-6.1.0
./configure --prefix=$HOME/gmp-6.1.0
make -j16
make install
make check >& a.log
```
注意prefix后的安装路径可以按照自己的实际情况更改，不需要与解压出的目录设成一致。`-j16`表示用16个线程并行编译，节约时间。`make check`这一步我们把编译日志重定向到文件a.log里，方便查看有无FAIL的情况。可以执行命令`grep '^# ERROR' a.log`从a.log查看错误数目。在确认没有错误后进行下一步。

# 2.安装MPFR
解压，编译
```
tar -jxf mpfr-4.0.0.tar.bz2
cd mpfr-4.0.0
./configure --prefix=$HOME/mpfr-4.0.0 --with-gmp=$HOME/gmp-6.1.0
make -j16
make install
```

# 3.安装MPC
解压，编译
```
tar -zxf mpc-1.1.0.tar.gz
cd mpc-1.1.0
./configure --prefix=$HOME/mpc-1.1.0 \
--with-gmp=$HOME/gmp-6.1.0 --with-mpfr=$HOME/mpfr-4.0.0
make -j16
make install
```

# 4.安装高版本GCC
先导出上述三个库的环境变量（普通用户在~/.bashrc里写，root用户可在/etc/bashrc等文件里写）
```
export LD_LIBRARY_PATH=$HOME/gmp-6.1.0/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$HOME/mpfr-4.0.0/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$HOME/mpc-1.1.0/lib:$LD_LIBRARY_PATH
```
并source使之生效。解压并进入gcc-5.4.0目录，编译
```
tar -zxf gcc-5.4.0.tar.gz
cd gcc-5.4.0
./configure --prefix=$HOME/gcc-5.4.0 \
--with-gmp=$HOME/gmp-6.1.0 \
--with-mpfr=$HOME/mpfr-4.0.0 \
--with-mpc=$HOME/mpc-1.1.0 \
--disable-multilib --enable-languages=c,c++,fortran \
--enable-checking=release
make -j16
make install
```
支持的编译器可以根据自己的需要增删，这里安装了三种c, c++和fortran。完成后导出gcc环境变量
```
export LD_LIBRARY_PATH=$HOME/gcc-5.4.0/lib64:$LD_LIBRARY_PATH
export PATH=$HOME/gcc-5.4.0/bin:$PATH
export CPATH=$HOME/gcc-5.4.0/include:$CPATH
```
并source使之生效。检查新安装的GCC版本号，依次执行
```
gcc -v
g++ -v
gfortran -v
```
注意安装完成后环境变量`CC`和`CXX`并没有自动更新至新的gcc编译器。如果用户之前没有定义过，这两个环境变量很有可能是空的。而有些软件在编译时会自动寻找`CC`和`CXX`，如果其无法自动识别出GCC-5.4.0，可以采取临时添加环境变量的方式，例如假设编译命令是`cmake ..`，那么可以按如下操作
```
CC=$HOME/gcc-5.4.0/bin/gcc CXX=$HOME/gcc-5.4.0/bin/g++ cmake ..
```
还有另一种办法是在~/.bashrc文件里写上这两环境变量
```
export CC=$HOME/gcc-5.4.0/bin/gcc
export CXX=$HOME/gcc-5.4.0/bin/g++
```
这样就永久生效了。

## 参考来源
https://blog.csdn.net/lll347/article/details/83060502

