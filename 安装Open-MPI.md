# 1 编译openmpi
到官网
```
https://www.open-mpi.org/software/ompi/v4.1
```
下载压缩包，笔者下载的压缩包为openmpi-4.1.1.tar.bz2。解压，进入程序目录
```
tar -jxf openmpi-4.1.1.tar.bz2
mv openmpi-4.1.1 openmpi
cd openmpi
```
此处将文件夹重命名不是必须的。笔者这么做目的是待编译成功后将openmpi文件夹和压缩包都删掉，只保留编译产生的可执行文件目录和库目录，节省硬盘空间。

接着从以下三种配置方式中选择一种：

（1）若想使用默认的GCC编译器编译Open MPI，则运行
```
./configure --prefix=$HOME/software/openmpi-4.1.1
```

（2）若安装了[高版本GCC](https://gitlab.com/jxzou/qcinstall/-/blob/main/Linux%E4%B8%8B%E5%AE%89%E8%A3%85%E9%AB%98%E7%89%88%E6%9C%ACGCC.md)，但发现没有被自动识别，可指明编译器路径，例如
```
./configure --prefix=$HOME/software/openmpi-4.1.1 \
FC=/opt/gcc-5.4.0/bin/gfortran CC=/opt/gcc-5.4.0/bin/gcc \
CXX=/opt/gcc-5.4.0/bin/g++
```

（3）若想使用Intel编译器编译Open MPI，则运行
```
./configure --prefix=$HOME/software/openmpi-4.1.1 FC=ifort CC=icc CXX=icpc
```

选择上述任一种配置方式后，运行
```
make -j24
make install
```
此处使用24核并行编译，读者需根据自己机器情况修改核数。完成后在`~/.bashrc`中写上环境变量
```
export PATH=$HOME/software/openmpi-4.1.1/bin:$PATH
export LD_LIBRARY_PATH=$HOME/software/openmpi-4.1.1/lib:$LD_LIBRARY_PATH
export CPATH=$HOME/software/openmpi-4.1.1/include:$CPATH
```
执行`source ~/.bashrc`或退出重登，以使环境变量生效。

# 2 可能遇到的问题
## 2.1 编译完成后在目标目录下找不到bin或lib
将上述`make -j24`和`make install`步骤再依次执行一遍即可。

## 2.2 make时屏幕上有如下类似报错
```
libtool: link: require no space between `-L' and `-lrt'
make[2]: *** [mca_pml_ucx.la] Error 1
make[2]: Leaving directory `/public/home/jxzou/software/openmpi/ompi/mca/pml/ucx'
make[1]: *** [all-recursive] Error 1
make[1]: Leaving directory `/public/home/jxzou/software/openmpi/ompi'
make: *** [all-recursive] Error 1
```

笔者的解决办法是自己编译一份ucx库，不使用机器上自带的。到[官网](https://github.com/openucx/ucx/releases)下载，笔者下载的压缩包是ucx-1.14.1.tar.gz，解压，编译
```
tar -zxf ucx-1.14.1.tar.gz
cd ucx-1.14.1
mkdir build && cd build
../configure --prefix=$HOME/software/ucx_1_14_1 --enable-mt CC=icc CXX=icpc
make -j24
make install
```
此处指定了Intel C/C++编译器，对应的是上述配置方式（3），读者可根据自己的需求搭配编译器。完成后在`~/.bashrc`文件里写上相应的环境变量
```
export PATH=$HOME/software/ucx_1_14_1/bin:$PATH
export CPATH=$HOME/software/ucx_1_14_1/include:$CPATH
export LD_LIBRARY_PATH=$HOME/software/ucx_1_14_1/lib:$LD_LIBRARY_PATH
```

执行`source ~/.bashrc`或退出重登，以使环境变量生效。然后删除编译失败的openmpi文件夹，回到本文开头重新编译即可。

